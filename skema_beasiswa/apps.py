from django.apps import AppConfig


class SkemaBeasiswaConfig(AppConfig):
    name = 'skema_beasiswa'
