from django.conf.urls import url
from .views import index, add_skema_beasiswa, add_beasiswa, skema_detail, daftar_beasiswa

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'add_new_schema/$', add_skema_beasiswa, name='add_skema_beasiswa'),
    url(r'add_beasiswa_to_schema/$', add_beasiswa, name='add_beasiswa_to_schema'),
    url(r'skema/(?P<id>\d+)/(?P<nourut>\d+)/$', skema_detail, name="skema_detail"),
    url(r'register_beasiswa/$', daftar_beasiswa, name='daftar_beasiswa'),
]
