from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.db import connection
from datetime import datetime
import os
import json

response = {}

def index (request):
    #Mengecek post dari user yang menyatakan
    #Apakah yang akan dilakukan oleh donatur?
    if request.method == 'POST':
        if request.POST['donatur_do'] == 'tambah_sb_baru':
            response['flag_new_schema'] = True
            return render (request, 'skema_beasiswa/donatur_do.html', response)
        elif request.POST['donatur_do'] == 'tambah_beasiswa_dari_sb':
            response['flag_new_beasiswa'] = True
            c = connection.cursor()
            c.execute("SELECT kode_skema_beasiswa FROM skema_beasiswa_aktif_skemabeasiswa")
            daftar_kode_beasiswa = dictfetchall(c)
            response['daftar_kode_beasiswa'] = daftar_kode_beasiswa
            return render (request, 'skema_beasiswa/donatur_do.html', response)
    else :
        response['flag_new_schema'] = False
        response['flag_new_beasiswa'] = False
        return render (request, 'skema_beasiswa/what_donatur_do.html', response)

@csrf_exempt
def add_skema_beasiswa (request):
    #Menerima post dari user
    if request.method == 'POST':
        kode_skema_beasiswa = request.POST['kode_skema_beasiswa']
        nama_skema_beasiswa = request.POST['nama_skema_beasiswa']
        jenis_skema_beasiswa = request.POST['jenis_skema_beasiswa']
        deskripsi = request.POST['deskripsi_beasiswa']
        syarat_beasiswa = request.POST['syarat_beasiswa']

        #CEK APAKAH KODE SKEMA BEASISWA TELAH TERDAFTAR DALAM DATABASE SISTEM
        c = connection.cursor()
        c.execute('SELECT kode_skema_beasiswa FROM skema_beasiswa_aktif_skemabeasiswa')
        daftarKodeSkema = dictfetchall(c)
        flagKodeSkema = any(i['kode_skema_beasiswa'] == kode_skema_beasiswa for i in daftarKodeSkema)
        c.close()

        #Jika belum terdaftar, masukkan kode skema beasiswa ke dalam database sistem
        if (not flagKodeSkema):
            nomor_identitas_donatur = request.session['nomor_identitas_donatur']
            c = connection.cursor()
            #Memasukkan skema beasiswa baru ke dalam tabel skema beasiswa
            query = "INSERT INTO skema_beasiswa_aktif_skemabeasiswa VALUES ('%s', '%s', '%s', '%s', '%s')" %\
            (kode_skema_beasiswa, nama_skema_beasiswa, jenis_skema_beasiswa, deskripsi, nomor_identitas_donatur)
            #Memasukkan syarat beasiswa tersebut ke dalam tabel syarat_beasiswa
            query2 = "INSERT INTO skema_beasiswa_aktif_syaratbeasiswa VALUES ('%s', '%s')" %(syarat_beasiswa, kode_skema_beasiswa)
            c.execute(query)
            c.execute(query2)
            c.close()
        return HttpResponseRedirect(reverse('skema_beasiswa:index'))
    else :
        return render (request, 'skema_beasiswa/what_donatur_do.html', request)

@csrf_exempt
def add_beasiswa (request):
    #menerima post data dari user
    if request.method == 'POST':
        postdata_kode_skema_beasiswa = request.POST['kode_skema_beasiswa']
        postdata_nomor_urut = request.POST['no_urut']
        postdata_tgl_mulai_pendaftaran = request.POST['tgl_mulai_pendaftaran']
        postdata_tgl_tutup_pendaftaran = request.POST['tgl_tutup_pendaftaran']
        postdata_periode_penerimaan = 'testing'
        postdata_status = 'ditutup'
        default_jumlah_pendaftar = 0
        nomor_identitas_donatur = request.session['nomor_identitas_donatur']

<<<<<<< HEAD
        #CEK KODE SKEMA DAN NOMOR URUT BERSIFAT UNIK
        c = connection.cursor()
        c.execute("SELECT * FROM skema_beasiswa_aktif_skemabeasiswaaktif")
        daftarKodeSkema = dictfetchall(c)
        flag = False
        for i in daftarKodeSkema :
            if (i['kode_skema_beasiswa_id'] == postdata_kode_skema_beasiswa and i['no_urut'] == postdata_nomor_urut):
                flag = True
        c.close()

        if (not flagKodeSkema) and (not flagNoUrut) :
            testing = 'testing'
            c = connection.cursor()
            query = "INSERT INTO skema_beasiswa_aktif_skemabeasiswaaktif(no_urut, tgl_mulai_pendaftaran, tgl_tutup_pendaftaran, periode_penerimaan, status, jumlah_pendaftar, kode_skema_beasiswa_id) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
            (postdata_nomor_urut, postdata_tgl_mulai_pendaftaran, postdata_tgl_tutup_pendaftaran, postdata_periode_penerimaan, postdata_status, default_jumlah_pendaftar, postdata_kode_skema_beasiswa)
            print(query)
            c.execute(query)
            c.close()
        return HttpResponseRedirect(reverse('skema_beasiswa:index'))


def skema_detail(request, id, nourut):
    c = connection.cursor()
    c.execute('SELECT * FROM skema_beasiswa_aktif_skemabeasiswaaktif join skema_beasiswa_aktif_skemabeasiswa ON kode_skema_beasiswa_id = kode_skema_beasiswa WHERE kode_skema_beasiswa_id = %s AND no_urut = %s', [id, nourut] )
    data_skema = dictfetchall(c)[0]
    print(data_skema)
    response['data_skema'] = data_skema
    c.close()
    return render(request, 'skema_beasiswa/detail_skema_beasiswa_aktif.html', response)

=======
        #Validasi nomor urut dan kode skema beasiswa bersifat unik, accepted = False jika tidak memenuhi, otherwise
        accepted = not (check_new_packet(postdata_kode_skema_beasiswa, postdata_no_urut))

        if (accepted):
          execute_insert_packet(postdata_kode_skema_beasiswa, postdata_no_urut, postdata_tgl_mulai_pendaftaran, postdata_tgl_tutup_pendaftaran, periode_penerimaan, status, default_jumlah_pendaftar)

        #Jika tidak accepted, tampilkan pesan error seperti yang diminta
        return HttpResponseRedirect(reverse('index'))

#Mengecek status paket beasiswa
def get_status (tgl_mulai_pendaftaran, tgl_tutup_pendaftaran):
    if (datetime.now() > tgl_mulai_pendaftaran and datetime.now() < tgl_tutup_pendaftaran):
        return 'dibuka'
    else :
        return 'ditutup''

#Mengecek apakah no urut dan kode skema beasiswa bersifat unik
def check_new_packet(kode_skema_beasiswa, no_urut):
    packet_ = execute_query("SELECT kode_skema_beasiswa, no_urut FROM skema_beasiswa_aktif WHERE kode_skema_beasiswa = '%s' and no_urut = '%s'" %(kode_skema_beasiswa, no_urut))
    if (packet_['no_urut'] == 'no_urut'):
        return True
    else :
        return False

#Memasukkan skema beasiswa ke dalam database sistem
def execute_insert_skema_beasiswa (kode, nama, jenis, deskripsi, nomor_identitas_donatur):
    execute_insert("INSERT INTO skema_beasiswa VALUES ('%s', '%s', '%s', '%s', '%s')" %(kode, nama, jenis, deskripsi, nomor_identitas_donatur))

#Memasukkan syarat beasiswa ke dalam database sistem
def execute_insert_syarat_beasiswa (kode_beasiswa, syarat):
    execute_insert("INSERT INTO syarat_beasiswa VALUES ('%s', '%s')" %(kode_beasiswa, syarat))

#Fungsi yang mengecek apakah kode skema beasiswa telah terdaftar dalam database sistem
def check_kode_skema_beasiswa (kode_skema_beasiswa):
    kode_skema_beasiswa_ = execute_query("SELECT kode FROM skema_beasiswa WHERE kode = '%s'" %(kode_skema_beasiswa))
    if (kode_skema_beasiswa_['kode'] == kode_skema_beasiswa):
        return True
    else :
        return False

#Mendapatkan seluruh daftar kode skema beasiswa
def get_daftar_kode_skema_beasiswa ():
    cursor = connection.cursor()
    c.execute("SELECT kode FROM skema_beasiswa")
    result = dictfetchall(cursor)
    c.close()
    return result

#Melihat Informasi Beasiswa Aktif - Fitur 4

#Fungsi yang menampilkan detail skema beasiswa tertentu sesuai yang diminta oleh user
def view_schema_detail(request, kode_beasiswa, no_urut):
    response['data_skema'] = execute_query("SELECT nama, tgl_tutup_pendaftaran, status, jumlah_pendaftar FROM skema_beasiswa_aktif JOIN skema_beasiswa ON kode = kode_skema_beasiswa WHERE kode = '%s' and no_urut = '%s'" %(kode_beasiswa, no_urut))
    return render(request, 'skema_beasiswa/detail_skema_beasiswa_aktif.html', response)

#Mendaftar Calon Penerima Beasiswa - Fitur 5

#Fungsi yang dipanggil ketika mahasiswa mendaftar beasiswa melalui form pendaftaran
def register_for_scholarship (npm, kode_skema_beasiswa):
    if request.method == "POST":
        postdata_kode_skema_beasiswa = request.POST['kode_skema_beasiswa']
        postdata_npm = request.POST['npm']
        postdata_email = request.POST['email']
        postdata_ips = request.POST['ips']

        #Validasi NPM dan kode skema beasiswa, has_been_registered = False jika mahasiswa dengan npm = npm belum pernah mendaftar beasiswa dengna kode = kode_skema_beasiswa
        has_been_registered = check_mahasiswa_has_been_registered_for_scholarship(postdata_kode_skema_beasiswa, postdata_npm)

        if not (has_been_registered) :
          execute_insert_pendaftaran(get_no_urut(postdata_kode_skema_beasiswa, postdata_kode_skema_beasiswa, postdata_npm, datetime.now(), 'Berhasil', 'Menunggu'))

#Fungsi yang melakukan cek apakah mahasiswa sudah pernah mendaftar suatu beasiswa
def check_student_has_been_registered_for_scholarship(kode_skema_beasiswa, npm):
    object_for_check = execute_query("SELECT kode_skema_beasiswa FROM pendaftaran WHERE kode_skema_beasiswa = '%s' and npm = '%s'" %(kode_skema_beasiswa, npm))
    if (object_for_check['npm'] == npm):
        return True
    else :
        return False

#Fungsi yang mengambil nomor urut skema beasiswa yang masih aktif dan paling awal
#Misalkan :
#A16 Foundation memiliki dua buah paket beasiswa
#Paket 1 masih dibuka
#Paket 2 masih dibuka
#Maka fungsi mengembalikan nomor urut paket 1 karena paket 1 lebih dulu dibandingkan paket 2
def get_no_urut (kode_skema_beasiswa):
    no_urut_ = execute_query("SELECT no_urut FROM skema_beasiswa_aktif WHERE status = 'dibuka' and kode_skema_beasiswa = '%s' LIMIT 1" %(kode_skema_beasiswa))
    return no_urut_['no_urut']
>>>>>>> e02922f1b288697c90a3775c6ab5a7fe86b4050a

def dictfetchall(cursor):
    desc = cursor.description
    return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
    ]


def daftar_beasiswa (request):
    if request.method == 'POST':
        postdata_kode_skema_beasiswa = request.POST['kode_skema_beasiswa']
        postdata_npm = request.POST['npm']

<<<<<<< HEAD
        if cek_mahasiswa_has_been_register_scholarship(postdata_npm, postdata_kode_skema_beasiswa):
            mahasiswa_daftar_beasiswa(postdata_npm, postdata_kode_skema_beasiswa)
        return HttpResponseRedirect(reverse('login:index'))
    else :
        c = connection.cursor()
        c.execute("SELECT npm, email FROM register_mahasiswa WHERE username_id = '%s'" %(request.session['username']))
        data = dictfetchall(c)[0]
        response['npm_pendaftar'] = data['npm']
        response['email_pendaftar'] = data['email']
        c.execute("SELECT kode_skema_beasiswa FROM skema_beasiswa_aktif_skemabeasiswa")
        return render(request, 'skema_beasiswa/form_pendaftaran_beasiswa.html', response)
=======
def set_accept(npm, waktu_daftar):
    c = connection.cursor()
    c.execute("UPDATE pendaftaran SET status_terima = 'Terima' WHERE npm = '%s' and waktu_daftar = '%s'" %(npm, waktu_daftar))
    c.close()
>>>>>>> e02922f1b288697c90a3775c6ab5a7fe86b4050a

#Cek apakah mahasiswa sudah mendaftar beasiswa tersebut
def cek_mahasiswa_has_been_register_scholarship (npm, kode_skema_beasiswa):
    c = connection.cursor()
<<<<<<< HEAD
    c.execute("SELECT npm, kode_skema_beasiswa FROM pendaftaran WHERE npm = '%s' AND kode_skema_beasiswa = '%s'" %(npm, kode_skema_beasiswa))
    list_npm_dan_kode_skema = dictfetchall(c)[0]
    c.close()

    if list_npm_dan_kode_skema == None :
        return False
    else :
        return True
=======
    c.execute("UPDATE pendaftaran SET status_terima = 'Menolak' WHERE npm = '%s' and waktu_daftar = '%s'" %(npm, waktu_daftar))
    c.close()

#@csrf_exempt
#def add_skema_beasiswa (request):
#    #Menerima post dari user
#    if request.method == 'POST':
#        kode_skema_beasiswa = request.POST['kode_skema_beasiswa']
#        nama_skema_beasiswa = request.POST['nama_skema_beasiswa']
#        jenis_skema_beasiswa = request.POST['jenis_skema_beasiswa']
#        deskripsi = request.POST['deskripsi_beasiswa']
#        syarat_beasiswa = request.POST['syarat_beasiswa']

#        #CEK APAKAH KODE SKEMA BEASISWA TELAH TERDAFTAR DALAM DATABASE SISTEM
#        c = connection.cursor()
#        c.execute('SELECT kode_skema_beasiswa FROM skema_beasiswa_aktif_skemabeasiswa')
#        daftarKodeSkema = dictfetchall(c)
#        flagKodeSkema = any(i['kode_skema_beasiswa'] == kode_skema_beasiswa for i in daftarKodeSkema)
#        c.close()

#        #Jika belum terdaftar, masukkan kode skema beasiswa ke dalam database sistem
#        if (not flagKodeSkema):
#            nomor_identitas_donatur = request.session['nomor_identitas_donatur']
#            c = connection.cursor()
#            #Memasukkan skema beasiswa baru ke dalam tabel skema beasiswa
#            query = "INSERT INTO skema_beasiswa_aktif_skemabeasiswa VALUES ('%s', '%s', '%s', '%s', '%s')" %\
#            (kode_skema_beasiswa, nama_skema_beasiswa, jenis_skema_beasiswa, deskripsi, nomor_identitas_donatur)
#            #Memasukkan syarat beasiswa tersebut ke dalam tabel syarat_beasiswa
#            query2 = "INSERT INTO skema_beasiswa_aktif_syaratbeasiswa VALUES ('%s', '%s')" %(syarat_beasiswa, kode_skema_beasiswa)
#            c.execute(query)
#            c.execute(query2)
#            c.close()
#        return HttpResponseRedirect(reverse('skema_beasiswa:index'))
#    else :
#        return render (request, 'skema_beasiswa/what_donatur_do.html', request)
#
#@csrf_exempt
#def add_beasiswa (request):
#    #menerima post data dari user
#    if request.method == 'POST':
#        postdata_kode_skema_beasiswa = request.POST['kode_skema_beasiswa']
#        postdata_nomor_urut = request.POST['no_urut']
#        postdata_tgl_mulai_pendaftaran = request.POST['tgl_mulai_pendaftaran']
#        postdata_tgl_tutup_pendaftaran = request.POST['tgl_tutup_pendaftaran']
#        postdata_periode_penerimaan = 'testing'
#        postdata_status = 'ditutup'
#        default_jumlah_pendaftar = 0
#        nomor_identitas_donatur = request.session['nomor_identitas_donatur']
#
#        #CEK KODE SKEMA DAN NOMOR URUT BERSIFAT UNIK
#        c = connection.cursor()
#        c.execute("SELECT * FROM skema_beasiswa_aktif_skemabeasiswaaktif")
#        daftarKodeSkema = dictfetchall(c)
#        flag = False
#        for i in daftarKodeSkema :
#            if (i['kode_skema_beasiswa_id'] == postdata_kode_skema_beasiswa and i['no_urut'] == postdata_nomor_urut):
#                flag = True
#        c.close()
#
#        if (not flagKodeSkema) and (not flagNoUrut) :
#            testing = 'testing'
#            c = connection.cursor()
#            query = "INSERT INTO skema_beasiswa_aktif_skemabeasiswaaktif(no_urut, tgl_mulai_pendaftaran, tgl_tutup_pendaftaran, periode_penerimaan, status, jumlah_pendaftar, kode_skema_beasiswa_id) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
#            (postdata_nomor_urut, postdata_tgl_mulai_pendaftaran, postdata_tgl_tutup_pendaftaran, postdata_periode_penerimaan, postdata_status, default_jumlah_pendaftar, postdata_kode_skema_beasiswa)
#            print(query)
#            c.execute(query)
#            c.close()
#        return HttpResponseRedirect(reverse('skema_beasiswa:index'))
#

#def skema_detail(request, id, nourut):
#    c = connection.cursor()
#    c.execute('SELECT * FROM skema_beasiswa_aktif_skemabeasiswaaktif join skema_beasiswa_aktif_skemabeasiswa ON kode_skema_beasiswa_id = kode_skema_beasiswa WHERE kode_skema_beasiswa_id = %s and no_urut = %s', [id, nourut] )
#    data_skema = dictfetchall(c)[0]
#    print(data_skema)
#    response['data_skema'] = data_skema
#    c.close()
#    return render(request, 'skema_beasiswa/detail_skema_beasiswa_aktif.html', response)


#def dictfetchall(cursor):
#    desc = cursor.description
#    return [
#            dict(zip([col[0] for col in desc], row))
#            for row in cursor.fetchall()
#    ]
#
#
#def daftar_beasiswa (request):
#    if request.method == 'POST':
#        postdata_kode_skema_beasiswa = request.POST['kode_skema_beasiswa']
#        postdata_npm = request.POST['npm']
#
#        if cek_mahasiswa_has_been_register_scholarship(postdata_npm, postdata_kode_skema_beasiswa):
#            mahasiswa_daftar_beasiswa(postdata_npm, postdata_kode_skema_beasiswa)
#        return HttpResponseRedirect(reverse('login:index'))
#    else :
#        c = connection.cursor()
#        c.execute("SELECT npm, email FROM register_mahasiswa WHERE username_id = '%s'" %(request.session['username']))
#        data = dictfetchall(c)[0]
#        response['npm_pendaftar'] = data['npm']
#        response['email_pendaftar'] = data['email']
#        c.execute("SELECT kode_skema_beasiswa FROM skema_beasiswa_aktif_skemabeasiswa")
#        return render(request, 'skema_beasiswa/form_pendaftaran_beasiswa.html', response)

#Cek apakah mahasiswa sudah mendaftar beasiswa tersebut
#def cek_mahasiswa_has_been_register_scholarship (npm, kode_skema_beasiswa):
#    c = connection.cursor()
#    c.execute("SELECT npm, kode_skema_beasiswa FROM pendaftaran WHERE npm = '%s' and kode_skema_beasiswa = '%s'" %(npm, kode_skema_beasiswa))
#    list_npm_dan_kode_skema = dictfetchall(c)[0]
#    c.close()

#    if list_npm_dan_kode_skema == None :
#        return False
#    else :
#        return True
>>>>>>> e02922f1b288697c90a3775c6ab5a7fe86b4050a

def mahasiswa_daftar_beasiswa (npm, kode_skema_beasiswa):
    status_daftar = 'Success'
    status_terima = 'Menunggu'
    waktu_daftar = datetime.datetime.now()
    c = connection.cursor()
    c.execute("SELECT no_urut FROM skema_beasiswa_aktif_skemabeasiswaaktif WHERE kode_skema_beasiswa_id = '%s'" %(kode_skema_beasiswa))
    no_urut = dictfetchall(c)[0].no_urut

    c.execute("INSERT INTO pendaftaran VALUES ('%s', '%s', '%s', '%s', '%s', '%s')" %(no_urut, kode_skema_beasiswa, npm, waktu_daftar, status_daftar, status_terima))
    c.close()
