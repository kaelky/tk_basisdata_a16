from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
from base.views import index, login, login_request, logout_request, register, add_mahasiswa, select_donatur_type, add_donatur_yayasan, add_donatur_individual


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^index/', index, name='index'),
    url(r'login/$', login, name='login'),

    url(r'login-request/$', login_request, name='login_request'),
    url(r'logout-request/$', logout_request, name='logout_request'),

    url(r'register/$', register, name='register'),
    url(r'register_mahasiswa/$', add_mahasiswa, name = 'add_mahasiswa'),
    url(r'select_donatur_type/$', select_donatur_type, name = 'select_donatur_type'),
    url(r'register_donatur_yayasan/$', add_donatur_yayasan, name='add_donatur_yayasan'),
    url(r'register_donatur_individual/$', add_donatur_individual, name='add_donatur_individual'),


    url(r'^skema_beasiswa/', include(('skema_beasiswa.urls', 'skema_beasiswa'), namespace='skema_beasiswa')),
    url(r'^$', RedirectView.as_view(url = '/index/', permanent = 'true'), name='home'),
]
