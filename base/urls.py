from django.conf.urls import url
from .views import index, login, login_request, logout_request

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'login/$', login, name='login'),
    url(r'login-request/$', login_request, name='login_request'),
    url(r'logout-request/$', logout_request, name='logout_request')
]
