from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.db import connection
from skema_beasiswa.views import dictfetchall
import os
import json

response = {}

def index (request):
    cursor = connection.cursor()
    response['list_beasiswa_aktif'] = fetch_skema_beassiwa_aktif()
    return render(request, 'base/home.html', response)

def login (request):
    if ('username' in request.session):
        return HttpResponseRedirect(reverse('index'))
    else:
        return render(request, 'base/login.html', response)

def login_request (request):
    if (request.method == 'POST'):
        postdata_username = request.POST['username']
        postdata_password = request.POST['password']

        cursor = connection.cursor()
        cursor.execute('SELECT * FROM register_pengguna WHERE username = %s', [postdata_username])
        data_pengguna = dictfetchall(cursor)[0]

        if (data_pengguna['password'] == postdata_password):
            request.session['username'] = data_pengguna['username']
            request.session['password'] = data_pengguna['password']
            request.session['role'] = data_pengguna['role']

            if (request.session['role'] == 'donatur') :
                c = connection.cursor()
                c.execute("SELECT nomor_identitas FROM register_donatur WHERE username_id = '%s'" %(postdata_username))
                no_id_donatur = dictfetchall(c)[0]
                request.session['nomor_identitas_donatur'] = no_id_donatur['nomor_identitas']
            response['username'] = data_pengguna['username']
            response['role'] = data_pengguna['role']
            return HttpResponseRedirect(reverse('index'))
    return HttpResponseRedirect(reverse('login'))

def logout_request (request):
    request.session.flush()
    return HttpResponseRedirect(reverse('index'))














############ util
def dictfetchall(cursor):
    desc = cursor.description
    return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
    ]

def fetch_skema_beassiwa_aktif ():
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM skema_beasiswa_aktif_skemabeasiswaaktif join skema_beasiswa_aktif_skemabeasiswa ON kode_skema_beasiswa_id = kode_skema_beasiswa')
    return dictfetchall(cursor)


############## register

def register (request):
    if request.method == 'POST':
        if request.POST['selected_role'] == 'mahasiswa':
            response['role'] = request.POST['selected_role']
            return render (request, 'base/register_mahasiswa.html', response)
        elif request.POST['selected_role'] == 'donatur':
            return render (request, 'base/register_pilih_tipe_donatur.html', response)
    else :
        return render (request, 'base/register_pilih_role.html', response)

@csrf_exempt
def add_mahasiswa (request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        npm = request.POST['npm']
        email = request.POST['email']
        nama = request.POST['nama']
        no_telp = request.POST['no_telp']
        alamat_tinggal = request.POST['alamat_tinggal']
        alamat_domisili = request.POST['alamat_domisili']
        nama_bank = request.POST['nama_bank']
        no_rekening = request.POST['no_rekening']
        nama_pemilik = request.POST['nama_pemilik']

        #CEK USERNAME DI DAFTAR USER
        c = connection.cursor()
        c.execute("SELECT username FROM register_pengguna")
        daftarPengguna = dictfetchall(c)
        c.execute("SELECT npm FROM register_mahasiswa")
        daftarMahasiswa = dictfetchall(c)
        c.close()

        flagUsername = any(i['username'] == username for i in daftarPengguna)
        flagNpm = any(j['npm'] == npm for j in daftarMahasiswa)

        #for daftar_nama_pengguna in daftarPengguna :
            #if username == daftar_nama_pengguna:
            #    flagUsername = True

        #Check that password have minimal eight character and any digit there
        flagPassword = any(i.isdigit() for i in password) and len(password) > 7

        if (not flagUsername) and flagPassword and (not flagNpm) :
            c = connection.cursor()
            query = "INSERT INTO register_pengguna VALUES ('%s', '%s', 'mahasiswa')" %\
            (username, password)
            c.execute(query)

            query = "INSERT INTO register_mahasiswa VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
            (npm, email, nama, no_telp, alamat_tinggal, alamat_domisili, nama_bank, no_rekening, nama_pemilik, username)
            c.execute(query)
            c.close()
        return HttpResponseRedirect(reverse('index'))

def select_donatur_type(request):
    if request.method == 'POST':
        if request.POST['selected_donatur_type'] == 'individual':
            response['donatur_type'] = request.POST['selected_donatur_type']
            return render (request, 'base/register_donatur.html', response)
        elif request.POST['selected_donatur_type'] == 'yayasan':
            response['donatur_type'] = request.POST['selected_donatur_type']
            return render (request, 'base/register_donatur.html', response)

@csrf_exempt
def add_donatur_individual(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nik = request.POST['nik']
        email = request.POST['email']
        nama = request.POST['nama']
        npwp = request.POST['npwp']
        no_telp = request.POST['no_telp']
        alamat = request.POST['alamat']

        #CEK USERNAME BELUM TERDAFTAR
        #Cek jumlah karakter pada password adalah minimal 8 karakter
        c = connection.cursor()
        c.execute("SELECT username FROM register_pengguna")
        daftarPengguna = dictfetchall(c)
        flagUsername = any(i['username'] == username for i in daftarPengguna)

        c.execute("SELECT nomor_identitas FROM register_donatur")
        daftarNomorIdentitas = dictfetchall(c)

        c.close()

        flagNomorIdentitas = any(j['nomor_identitas'] == nomor_identitas for j in daftarNomorIdentitas)

        #Check that password have minimal eight character and any digit there
        flagPassword = any(i.isdigit() for i in password) and len(password) > 7

        if (not flagUsername) and flagPassword and (not flagNomorIdentitas) :
            c = connection.cursor()
            query = "INSERT INTO register_pengguna VALUES ('%s', '%s', 'donatur')" %\
            (username, password)
            c.execute(query)
            c.close()

        #Cek Apakah nomor Identitas Donatur sudah terdaftar
        #Cek apakah nik donatur sudah terdaftar
        #Cek apakah jumlah karakter NIK donatur adalah tepat 16 buah

        c = connection.cursor()
        c.execute("SELECT nik FROM register_individual_donor")
        daftarNIK = dictfetchall(c)
        c.close()
        flagNIK = any(k['nik'] == nik for k in daftarNIK)
        flagNIK_jumlah_karakter = len(nik) == 16

        if (not flagNIK and flagNIK_jumlah_karakter ):
            c = connection.cursor()
            query = "INSERT INTO register_donatur VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
            (nomor_identitas, email, nama, npwp, no_telp, alamat, username)
            query2 = "INSERT INTO register_individual_donor VALUES ('%s', '%s')" %\
            (nik, nomor_identitas)
            c.execute(query)
            c.execute(query2)
            c.close()

        return HttpResponseRedirect(reverse('index'))

@csrf_exempt
def add_donatur_yayasan (request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nomor_sk_yayasan = request.POST['nomor_sk_yayasan']
        email = request.POST['email']
        nama = request.POST['nama']
        no_telp = request.POST['no_telp']
        npwp = request.POST['npwp']
        alamat = request.POST['alamat']

        #CEK USERNAME BELUM TERDAFTAR
        #Cek jumlah karakter pada password adalah minimal 8 karakter
        c = connection.cursor()
        c.execute("SELECT username FROM register_pengguna")
        daftarPengguna = dictfetchall(c)

        flagUsername = any(i['username'] == username for i in daftarPengguna)

        c.execute("SELECT nomor_identitas FROM register_donatur")
        daftarNomorIdentitas = dictfetchall(c)

        flagNomorIdentitas = any(j['nomor_identitas'] == nomor_identitas for j in daftarNomorIdentitas)
        c.close()

        #Check that password have minimal eight character and any digit there
        flagPassword = any(i.isdigit() for i in password) and len(password) > 7

        if (not flagUsername) and flagPassword and (not flagNomorIdentitas) :
            c = connection.cursor()
            query = "INSERT INTO register_pengguna VALUES ('%s', '%s', 'donatur')" %\
            (username, password)
            c.execute(query)
            c.close()

        c = connection.cursor()
        c.execute("SELECT no_sk_yayasan FROM register_yayasan")
        daftar_nomor_SK_yayasan = dictfetchall(c)
        flagSK = any(k['no_sk_yayasan'] == nomor_sk_yayasan for k in daftar_nomor_SK_yayasan)

        if (not flagSK):
            c = connection.cursor()
            query = "INSERT INTO register_donatur VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
            (nomor_identitas, email, nama, npwp, no_telp, alamat, username)
            query2 = "INSERT INTO register_yayasan VALUES ('%s', '%s', '%s', '%s', '%s')" %\
            (nomor_sk_yayasan, email, nama, no_telp, nomor_identitas)
            c.execute(query)
            c.execute(query2)
            c.close()

        return HttpResponseRedirect(reverse('index'))
